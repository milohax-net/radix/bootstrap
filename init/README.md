# This directory is not in use

It holds old initialisation bash scripts from Cowboy setup, which may be
helpful information for general bootstrapping, but is being replaced
with other mechanisms.

Eventually this directory should be pruned.
