#!/usr/bin/env bash

## Utility functions
is_osx() {
  [[ "$(uname)" =~ ^[Dd]arwin ]] || return 1
}
is_suse() {
  grep -i SUSE /etc/os-release 2> /dev/null || return 1
}
error() {
  echo ${@} 1>& 2
}

## Make scaffolding

[[ -n $XDG_CONFIG ]] || export XDG_CONFIG=~/.config
[[ -s ~/etc ]] || ln -s $XDG_CONFIG ~/etc

for DIRECTORY in ~/lib/ansible ~/$XDG_CONFIG; do
  [[ -d ${DIRECTORY} ]] || mkdir -p ${DIRECTORY}
done

## Install system Ansible and base developer requirements

cat << INTRO

------------------------------------------------------------------------

                  INSTALLING BASE SYSTEM REQUIREMENTS

  When prompted for Password: use your root-level access password.

------------------------------------------------------------------------

INTRO

read -p "Press enter to continue, or Ctl-C to break."

if is_osx; then
  xcode-select --install
  /usr/bin/python3 -m pip install --upgrade --user \
    --no-warn-script-location \
    pip ansible
  # On a Mac, `pip install -user` installs to `~/Library/Python/3.*/bin`
  export PATH="$(dirname ~/Library/Python/*/bin/ansible):${PATH}"
elif is_suse; then
  sudo zypper --quiet refresh
  sudo zypper install -y \
    pattern:console \
    pattern:devel_basis \
    pattern:devel_python3 \
    ansible git-core
else
  error Bootstrap supports Mac or SUSE. Exiting
  exit 1
fi

## Clone radix libraries
# Will use HTTPS to get them without SSH keys
# Can switch later:
#  git remote set-url origin git@gitlab.com:${USER}/${NAMESPACE}.git

git clone https://gitlab.com/milohax/workstation.git \
  ~/lib/ansible/workstation
git clone https://gitlab.com/milohax-net/radix/bashing.git ~/lib/bash
git clone https://gitlab.com/milohax-net/radix/fishing.git ~/lib/fish
git clone https://gitlab.com/milohax-net/radix/dotfiles.git ~/etc/dotfiles

## Rosetta

[[ $(uname -m) =~ arm ]] && is_osx && sudo softwareupdate --install-rosetta

# Check out os-specific branch and install requirements

pushd ~/lib/ansible/workstation
  is_osx && git checkout mac
  is_suse && git checkout suse
  ansible-galaxy install -r requirements.yml
popd

cat MANUAL_STEPS.md
