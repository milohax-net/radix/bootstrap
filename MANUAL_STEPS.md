************************************************************************

READY TO RUN bootstrap Ansible playbook.
===

First, if on a Macintosh, run this (in `zsh`):

  ```sh
  [[ "${OSTYPE}" =~ ^darwin ]] \
    && path+=($HOME/Library/Python/*/bin /opt/homebrew/bin)
  ```

To execute the playbook:
  ```sh
  cd ~/lib/ansible/workstation
  ansible-playbook main.yml --ask-become-pass
  ```

* This can be repeated (it's idempotent)
* You can repeat just parts with `--tags` option (e.g. `--tags homebrew`)
* `bootstrap.sh` does not need to be run again

OTHER MANUAL STEPS:
---

* Login and sync Firefox and VSCode

* The dotfiles/shell libraries have been cloned with HTTPS. Switch them to SSH:
  ```sh
  git remote set-url origin git@gitlab.com:${USER}/${NAMESPACE}.git
  ```
* Fix Xcode being asked to reinstall all the time:
  ```sh
  sudo xcode-select -switch /Library/Developer/CommandLineTools
  ```
* Keyboard tweaks on a Macintosh:
  - Open System Preferences
  - Add Dvorak keyboard layout (Keyboard > Input Sources > + > English/Dvorak)
  - Switch Caps lock to Command (Keyboard > Modifier Keys...)
  
* (optional) Once switched to ASDF and virtualenv, remove bootstrap Python:

  - Remove user-python from your Macintosh:
    ```sh
    rm -rf ~/Library/Python
    ```
  - Remove global ansible from openSUSE:
    ```sh
    sudo zypper rm ansible
    ```
