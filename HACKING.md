# Hacking on a Workstation

Some notes on how to pull together these Radix projects into a system of tools for my workstation setup.

A Workstation is a computer where I do my work. In the past it's been a PC running OpenSUSE, now I mainly work on a Macintosh. I like both of these environments, and they have different strengths and weaknesses.

One thing all my workstations have in common: they are Unix at the core.

Recently, Windows 10 has become very useable, with its Windows Subsystem for Linux, and some of this project may evolve to set up a Windows Workstation, with the new subsystem configured in a similar way to OpenSUSE.

## Directories

With the workstation settings and tools spread across different GitLab Projects and git repositories, it's important to have a clear structure to bring it all together. Also, [Linux/SystemD is moving to having a small encrypted `$HOME` filesystem](https://systemd.io/HOME_DIRECTORY/), which won't have room to store programming environments directly. So [my old conventions](https://milosophical.me/blog/2016/home-dir-maintenance.html) are going to be helpful:

 * `bak`: backup files
 * `bin`: personal programs (shell, python, ruby, etc)
 * `etc`: settings ("dotfiles"/dotdirs)
 * `fun`: games
 * `hax`: personal hacking projects (github, personal lab)
 * `img`: disc images
 * `key`: encryption keys
 * `lab`: work projects (gitlab)
 * `lib`: programming libraries and runtimes
 * `mnt`: place to mount filesystems
 * `net`: links to network drives
 * `srv`: place to serve files from
 * `tmp`: temporary files
 * `vms`: place to store virtual machines/boxes

These are either directories with separate volumes mounted to them, or they are symlinks to other locations in the workstation's storage.

Key directories are `bin`, `etc`, `hax`, `lab`, `lib`, and `tmp`.

## Conventions

- Use three-letterisms for the key directories/locations
- Maintain the platform's long-form "user friendly" names for data storage, rather than try to make macOS/Linux/Windows all conform
- `~/etc` is a symlink to `${XDG_CONFIG}` (which is `~/.config` by convention)
- Set all temporary variables to `~/tmp` to maintain user privacy. Also `~/tmp` should block world-read/write/execute

## Which repository?

- `bootstrap` is for configuring the workstation "base". Add base depencies and configuration for the applications in here
- `workstation` is for provisioning a workstation using Ansible. It is configured from `bootstrap`

The other repositories are installed by the `workstation` playbook:
- `dotfiles` is for cross-platform application and general configuration settings
- `bashing` is for bash shell configuration, and bash scripts
- `fishing` is for fish shell configuration, and fish scripts


## Packaging systems

I like to use the workstation platform's main packaging system, if it has one. On openSUSE this is RPM, managed through Zypper. There is also Snap, which is a cross-distro packaging system that I have mixed feelings about. For now (2022/2023) I am not adding it to this bootstrap, because the only snap I would use is Spotify, can just use the web interface. It's getting more use in other distributions, notably Ubuntu, so things may change.

Neither Windows or macOS has an _official_ packaging system, instead there are choices. The most popular (and therefore easily supported) choice on macOS is _Homebrew_. For Windows, _Chocolatey_ seems to be popular.

For all systems, packages can be upgraded without notice. This can have an impact to development if dependencies are broken in the upgrade. So for this reason it's important to avoid using "system" dependencies in projects. Instead use a _virtualisation_ solution:

- Vagrant-managed VMs (via VirtualBox, Xen, KVM, Hyper-V/WSL2)
- Containers (via Docker/Rancher/containerd, podman, kubernetes)
- Multiple language runtimes (via ASDF)
- Language 'virtual environments', or library bundling (where offered)
