# Bootstrap

This project bootstraps a workstation with the minimum requirements for building a `$HOME` directory using the other [Radix](https://gitlab.com/milohax-net/radix/) projects:

* On macOS, [Homebrew](https://brew.sh/)
* On Windows, [Chocolatey](https://chocolatey.org/)
* The latest [Git](https://git-scm.com/) DVCS 
* [Anaconda](https://www.anaconda.com/)
* On macOS and openSUSE:
  * [ASDF](https://asdf-vm.com/#/)/[python](https://github.com/danhper/asdf-python)
  * [Ansible](https://www.ansible.com/)

After the requirements for Ansible and a packaging system are in place, the bootstrap will use it to clone and install the remaining projects:

 * Clone [ansible](https://gitlab.com/milohax-net/radix/ansible) (the plays/roles to actually install the other projects)
 * Install [dotfiles](https://gitlab.com/milohax-net/radix/dotfiles) (application settings and global preferences)
 * Install [bashing](https://gitlab.com/milohax-net/radix/bashing) (bash functions/aliases/completions)
 * Install [fishing](https://gitlab.com/milohax-net/radix/ansible) (fish functions/aliases/completions)

## Bootstrapping a workstation

This `bootstrap` project's purpose is to:

1. Identify the workstation's operating system
1. For macOS and Windows, Install a packaging manager ([Homebrew for macOS](https://brew.sh/) and [Chocolately for Windows](https://chocolatey.org/))
1. Install the latest Git using the package manager
1. For macOS and openSUSE Only:
   1. Install [Ansible](https://www.ansible.com/) (with system python/package manager)
   1. Clone my [ansible project](https://gitlab.com/milohax-net/radix/ansible)
   1. Clone other radix shell/dotfiles projects
   1. Use the ansible plays to install the dotfiles, and remaining packages and applications
   1. Install [ASDF](https://asdf-vm.com/), the [Python plugin](https://github.com/danhper/asdf-python), and Python 3.latest-minus-one
1. Install [Anaconda](https://www.anaconda.com/) ([from the web](https://www.anaconda.com/products/individual#Downloads))

### Why use Anaconda?

Because it's the simplest way to install a complete, working python system in Windows. Even though Ansible isn't supported as a control node for Windows, I do still want/need the Anaconda ecosystem for doing python windows hacking.

### Workstation Operating Systems

I use [Apple macOS](https://www.apple.com/macos/) primarily (for work), and a dual-boot [openSUSE](https://www.opensuse.org/) and [Microsoft Windows](https://www.microsoft.com/en-us/windows) PC. For workstations which are running a Unix/Linux, they will be Ansible control nodes to other Linux containers and grid nodes.

This `bootstrap` project is only concerned with setting up a workstation running one of those three operating systems, and the other projects are only concerned with their own domain within a Unix system of some kind.
